# Excercise Cruise Control

The idea is to simulate a simplified version of a adaptive cruise control.

## The Givens

Given a file with a list of velocities. E.g. : 

```
0
10
20
30
35
46
54
50
50
60
40
28
23
25
15
9
4
0
```

Meaning the speed at t=0 is 0, t=1 is 10 km/h t=2 20 km/h etc.
These would be the velocities of the car in front of you.

We further assume:

    1. At t=0 both the vehicle in front of you as well as your own is standing still with a distance of 2 meters in between (between back of the car in front and the front of your car)

    2. There is a sensor in the front of the car cat measures the distance between the cars.

    3. Max acceleration is 4 m/s

    4. Max deceleration (breaking) is 10 m/s

    5. In between time intervals the acceleration is constant.

    6. At any point t, the cars can change their acceleration and consequently their velocity.

## Assignment

Make a program that reads the file and parses it's data into useful values for the program.

Make an algorithm that makes sure the car follows the lead car, but does not crash into it.

Print out the distances and velocities of both cars over time.

Make a function / test that checks if the cars crashed.

## Pay attention to

Pay attention to any and all of the following subjects:

    1. structure of the project

    2. Structure of the solution

    3. Code should run

    4. Clear naming

    5. Clean code

    5. Clear git commits

## Good luck !

See how far you can get, take approximately an hour. First calmly think about it, then we will have a short call where you can ask any questions you might have. 

After that create a git repository and initialize with a first commit. Work on your solution, making frequent commits as you go, so we can follow along your thinking. The last commit should be more or less an hour in. If you're done early: great if not, try to finish the part you a re working on and submit it. Going over time does not make you fail anything, it is simply not our intention to have you work for nothing a long time.

Good luck!